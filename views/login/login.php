<div class="row logo-container">
	<div class="col-md-12">
		<div id="trustlogo">

		</div>

	</div>

</div>

<div class="row">
	<div class="col-md-12">
		<?php echo $messages; ?>
	</div>
</div>

<div class="row">
		<nav>
		<ul class="nav nav-tabs" id="settingsTabs">
		  <li class="active general"><a href="#SettingsGeneral">Login/Register</a></li>
		</ul>
	</nav>
	<div id="container">
		
		<div class="col-md-4 col-md-offset-2">
			<form method="post" action="/authenticate/login">
				<fieldset>
					<legend>Login</legend>
					<div class="row">
						<div class="col-md-12">
							<label for="login_username"><?php echo __('login.form.username'); ?></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input type="text" id="login_username" name="username" value="<?php Input::post('username'); ?>">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label for="login_password"><?php echo __('login.form.password'); ?></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input type="password" name="password" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input type="submit" name="login" class="btn btn-success" value="<?php echo __('login.form.button.login'); ?>"/>
						</div>
					</div>

			<div class="row">
			<div class="col-md-12">
				<p><a id="fbook" href="/authenticate/oauth/facebook"></a></p>
			</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p><a id="twitter" href="/authenticate/oauth/twitter"></a></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p><a id="" href="/authenticate/forgotten">Forgotten Password?</a></p>
				</div>
			</div>
			</fieldset>
		</form>

	</div>

	<div class="col-md-4">
		<form method="post" action="/authenticate/register">
			<fieldset>
				<legend>Register</legend>
				<?php echo $form; ?>
		</form>
	</div>
</div>
</div>