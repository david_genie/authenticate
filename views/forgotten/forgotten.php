<div class="row logo-container">
	<div class="col-md-12">
		<div id="trustlogo">

		</div>

	</div>

</div>

<div class="row">
	<div class="col-md-12">
		<?php echo $messages; ?>
	</div>
</div>

<div class="row">
		<nav>
		<ul class="nav nav-tabs" id="settingsTabs">
		  <li class="active general"><a href="#SettingsGeneral">Password Reset</a></li>
		</ul>
	</nav>
	<div id="container">
		
		<div class="col-md-8 col-md-offset-2">
			<h3>Password Reset</h3>
			<form method="post" action="" class="form-horizontal">
				<fieldset>
					
					<?php if ($passwordResetFrom): ?>
						<p>Enter your new password and the confirmation of that password below.</p>
						<div class="form-group" id="">
					    <label for="inputEmail3" class="col-sm-4 control-label"><?php echo __('forgotten.form.password'); ?></label>
					    <div class="col-sm-5">
					      <input type="password" class="form-control" placeholder="" name="password">
					    </div>
					  </div>
						<div class="form-group" id="">
					    <label for="inputEmail3" class="col-sm-4 control-label"><?php echo __('forgotten.form.confirm'); ?></label>
					    <div class="col-sm-5">
					      <input type="password" class="form-control" placeholder="" name="confirm">
					    </div>
					  </div>
						<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>" />
						<div class="form-group">
					    <div class="col-sm-offset-2 col-sm-4">
								<input type="submit" class="btn btn-success" name="reset_password" value="<?php echo __('forgotten.form.reset'); ?>" id="reset">
					    </div>
					  </div>
					<?php else: ?>
						<p>To reset your password enter your email address below. We'll send an email with a reset url.</p>
						<p>By navigating to the reset url you will be able to enter a new password.</p>
						<div class="form-group" id="reset_email">
					    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo __('forgotten.form.email_address'); ?></label>
					    <div class="col-sm-8">
					      <input type="email" class="form-control" id="inputEmail3" placeholder="Email Address" name="email_address">
					    </div>
					  </div>
						<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>" />
						<div class="form-group">
					    <div class="col-sm-offset-2 col-sm-4">
								<input type="submit" class="btn btn-success" name="reset" value="<?php echo __('forgotten.form.reset'); ?>" id="reset">
					    </div>
					    <div class="col-sm-4">
								<a href="/dashboard" class="btn" style="margin-top: 4px;">Cancel</a>
					    </div>
					  </div>
					<?php endif ?>					
				</fieldset>
			</form>
		</div>

	</div>
</div>