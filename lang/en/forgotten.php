<?php
return array(
	'form' => array(
		'email_address' => 'Email',
		'reset' => 'Reset Password',
		'password' => 'New Password',
		'confirm' => 'Confirm New Password'
	),
	'failed' => array(
		'validation' => 'Please ensure a valid email address is provided.',
		'reset_password' => 'Please ensure that all fields are completed and that the password and confirmation match.',
		'security' => 'Security failed.'
	),
	'success' => array(
		'sent' => 'The email has been successfully sent to :email_address',
	),
	'email' => array(
		'from' => array(
			'address' => 'no-reply@trustev-shopify-app.com'
		),
		'body' => "Hi,\n\nPlease find below the url to reset your password for the Trustev Shopify App.\n\nUrl: :url",
	),
	'token' => array(
		'expired' => 'The url has expired. Please enter your email address to reset your password and generate a new url.'
	),
	'password' => array(
		'changed' => 'Your password has been successfully changed.',
		'not_changed' => 'The password could not be changed.',
	),
	'user' => array(
		'not_found' => 'The user could not be found to reset the password.',
	),
);