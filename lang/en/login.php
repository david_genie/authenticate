<?php
return array(
	'already-logged-in'					=> 'You are already logged in',
	'failure'							=> 'Invalid username / password. Please try again.',
	'registration-not-enabled'			=> 'Registration has been disabled',
	'form' => array(
		'username'						=> 'Username',
		'password'						=> 'Password',
		'fullname'						=> 'Full name',
		'confirm'						=> 'Confirm password',
		'email'							=> 'Email',
		'button' => array(
			'login'						=> 'Login',
			'register'					=> 'Register',
		)
	),
	'validation-fail' 					=> 'Please ensure that all fields are populated.',
	'new-account-created'				=> 'Your registraton has been successful',
	'account-creation-failed'			=> 'Your account could not be created',
	'email-already-exists'				=> 'An account already exists with that email address',
	'username-already-exists'			=> 'That username is already in use',
	'password-recovery'					=> 'Password reset request',
	'invalid-email-address'				=> 'Invalid email address',
	'error-sending-email'				=> 'There was an error sending the email',
	'error-missing-email'				=> 'You have not specified your email address',
	'recovery-email-send'				=> 'Password reset instructions have been sent to your email address',
	'password-recovery-accepted'		=> 'Welcome back, please change your password',
	'recovery-hash-invalid'				=> 'Invalid password reset request',
	'logged-out'						=> 'You have been logged out',
	'provider-linked'					=> 'Your account has been linked',
	'register-first'					=> 'In order to login using your %s account, you will need to complete the registration first',
	'logged_in_using_provider'			=> 'You have been logged in using %s',
	'auto-registered'					=> 'You have been automatically registered',
	'no-provider-specified'				=> 'No login provider has been specified'
);