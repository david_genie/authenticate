<?php
return array(
	'authenticate/login'		=> 'authenticate/authenticated/login',
	'authenticate/lostpassword'	=> 'authenticate/authenticated/lostpassword',
	'authenticate/register'		=> 'authenticate/authenticated/register',
	'authenticate/logout'		=> 'authenticate/authenticated/logout',
	'authenticate/oauth/(:any)'	=> 'authenticate/authenticated/oauth/$1',
	'authenticate/callback'		=> 'authenticate/authenticated/callback',
	'authenticate/forgotten' => 'authenticate/authenticated/forgotten',
	'authenticate/forgotten/(:any)' => 'authenticate/authenticated/forgotten/$1',
);
