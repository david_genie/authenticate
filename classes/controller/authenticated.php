<?php
namespace Authenticate;

class Controller_Authenticated extends \Controller_Hybrid {

	public $template = 'template';

	/*
	 * This controller is resticted to authenticated users only
	 */
	protected $requires_authentication = false;

	/*
	 * These actions are available without authentication
	 */
	protected $public_actions = array(
		'login',
		'register',
		'lostpassword',
		'oauth',
		'callback',
	);

	public function before() {
				
		parent::before();
		
		// load any events
		\Config::load('event');
		
		if (is_array(\Config::get('event'))) {
			foreach (\Config::get('event') as $event => $callback) {
				\Event::register($event, $callback);
			}
		}
		
		if ($this->requires_authentication) {			
			if ( ! \Auth::check() && ! in_array(\Request::active()->action, $this->public_actions)) {
				// redirect the user to the log in page
				\Response::redirect('/authenticate/login');
			}			
		}

		\Lang::load('authenticate::login', 'login');
		\Lang::load('authenticate::forgotten', 'forgotten');
	}

	public function action_login()
	{
		// already logged in?
		// if (\Auth::check())
		// {
		// 	// yes, so go back to the page the user came from, or the
		// 	// application dashboard if no previous page can be detected
		// 	\Message::info(__('login.already-logged-in'));
		// 	\Response::redirect_back('dashboard');
		// }

		// was the login form posted?
		if (\Input::method() == 'POST')
		{
			// check the credentials.
			if (\Auth::instance()->login(\Input::param('username'), \Input::param('password')))
			{
				// did the user want to be remembered?
				if (\Input::param('remember', false))
				{
					// create the remember-me cookie
					\Auth::remember_me();
				}
				else
				{
					// delete the remember-me cookie if present
					\Auth::dont_remember_me();
				}
				
				// check whether there's a redirect uri in the session
				$redirect_uri = \Session::get('redirect_uri', 'dashboard');
				
				// delete the redirect uri from the session as it's no longer needed
				\Session::delete('redirect_uri');

				// logged in, go back to the page the user came from, or the
				// application dashboard if no previous page can be detected
				\Response::redirect_back($redirect_uri);
			}
			else
			{
				// login failed, show an error message
				\Message::error(__('login.failure'));
			}
		}

		$form = $this->getRegistrationForm();

		// display the login page
		$view = \View::forge('authenticate::login/login')->set('form', $form, false);
		$view->set('messages', \Message::get(), false);

		$this->template->pageTitle = "Login";
		$this->template->content = $view;
		$this->template->set('messages', \Message::get(), false);
	}

	public function action_register()
	{
		// is registration enabled?
		if ( ! \Config::get('application.user.registration', false))
		{
			\Log::warning(__('login.registration-not-enabled'));

			// inform the user registration is not possible
			\Message::error(__('login.registration-not-enabled'));

			// and go back to the previous page (or the homepage)
			\Response::redirect_back();
		}

		$form = $this->getRegistrationForm();


		// fetch the oauth provider from the session (if present)
		$provider = \Session::get('auth-strategy.authentication.provider', false);

		// if we have provider information, create the login fieldset too
		if ($provider)
		{
			// disable the username, it was passed to us by the Oauth strategy
			// $form->field('username')->set_attribute('readonly', true)->delete_rule("required");

			// create an additional login form so we can link providers to existing accounts
			$login = \Fieldset::forge('loginform');
			$login->add_model('Model\\Auth_User');
			// we only need username and password
			$login->disable('group_id')->disable('email');
		}

		// was the registration form posted?
		if (\Input::method() == 'POST')
		{
			// was the login form posted?
			if ($provider and \Input::post('login'))
			{
				// check the credentials.
				if (\Auth::instance()->login(\Input::param('username'), \Input::param('password')))
				{
					// get the current logged-in user's id
					list(, $userid) = \Auth::instance()->get_user_id();

					// so we can link it to the provider manually
					$this->link_provider($userid);

					// inform the user we're linked
					\Message::success(sprintf(__('login.provider-linked'), ucfirst($provider)));
				
					// logged in, go back where we came from,
					// or the the user dashboard if we don't know
					\Response::redirect(\Router::get('dashboard'));
				}
				else
				{
					// login failed, show an error message
					\Message::error(__('login.failure'));
				}
			}

			// was the registration form posted?
			elseif (\Input::post('register'))
			{
				\Log::info("Registration form submitted");

				// validate the input
				$form->validation()->run();

				// if validated, create the user
				if ( ! $form->validation()->error())
				{
					try
					{
						// emit the registration event so that any observers can run
						\Event::trigger('user_registration', $form->validated());
						
						// call Auth to create this user
						$created = \Auth::create_user(
							$form->validated('username'),
							$form->validated('password'),
							$form->validated('email'),
							\Config::get('application.user.default_group', 1),
							array(
								'fullname' => $form->validated('fullname'),
							)
						);

						// if a user was created succesfully
						if ($created)
						{
							// log the user in
							\Auth::instance()->force_login($created);
							
							// inform the user
							\Message::success(__('login.new-account-created'));

							// and go back to the previous page, or show the
							// application dashboard if we don't have any
							\Response::redirect(\Router::get('dashboard'));
						}
						else
						{
							// oops, creating a new user failed?
							\Message::error(__('login.account-creation-failed'));
						}
					}

					// catch exceptions from the create_user() call
					catch (\SimpleUserUpdateException $e)
					{
						// duplicate email address
						if ($e->getCode() == 2)
						{
							\Message::error(__('login.email-already-exists'));
						}

						// duplicate username
						elseif ($e->getCode() == 3)
						{
							\Message::error(__('login.username-already-exists'));
						}
						
						// shop already exists
						elseif ($e->getCode() == 4)
						{
							\Message::error($e->getMessage());
						}

						// this can't happen, but you'll never know...
						else
						{
							\Message::error($e->getMessage());
						}
					}
				} else {
					\Log::info('Failed validation of registration form');
					$error = $form->validation()->error();
					\Message::error(__('login.validation-fail'));
				}
			}

			// validation failed, repopulate the form from the posted data
			$form->repopulate();
		}
		else
		{
			// get the auth-strategy data from the session (created by the callback)
			$user_hash = \Session::get('auth-strategy.user', array());

			// populate the registration form with the data from the provider callback
			$form->populate(array(
				'username' => \Arr::get($user_hash, 'nickname'),
				'fullname' => \Arr::get($user_hash, 'name'),
				'email' => \Arr::get($user_hash, 'email'),
			));
		}

		// pass the fieldset to the form, and display the new user registration view
		$view = \View::forge('authenticate::login/login');
		$view->set('form', $form, false);
		$view->set('login', isset($login) ? $login : null, false);

		$view->set('messages', \Message::get(), false);

		$this->template->pageTitle = 'Login';
		$this->template->content =  $view;
		$this->template->set('messages', \Message::get(), false);
	}

	public function action_lostpassword($hash = null)
	{
		// was the lostpassword form posted?
		if (\Input::method() == 'POST')
		{
			// do we have a posted email address?
			if ($email = \Input::post('email'))
			{
				// do we know this user?
				if ($user = \Model\Auth_User::find_by_email($email))
				{
					// generate a recovery hash
					$hash = \Auth::instance()->hash_password(\Str::random()).$user->id;

					// and store it in the user profile
					\Auth::update_user(
						array(
							'lostpassword_hash' => $hash,
							'lostpassword_created' => time()
						),
						$user->username
					);

					// send an email out with a reset link
					\Package::load('email');
					$email = \Email::forge();

					// use a view file to generate the email message
					$email->html_body(
						\Theme::instance()->view('authenticate::login/lostpassword')
							->set('url', \Uri::create('login/lostpassword/'.$hash), false)
							->set('user', $user, false)
							->render()
					);

					// give it a subject
					$email->subject(__('login.password-recovery'));

					// add from- and to address
					$from = \Config::get('application.email-addresses.from.website', 'website@example.org');
					$email->from($from['email'], $from['name']);
					$email->to($user->email, $user->fullname);

					// and off it goes (if all goes well)!
					try
					{
						// send the email
						$email->send();
					}

					// this should never happen, a users email was validated, right?
					catch(\EmailValidationFailedException $e)
					{
						\Message::error(__('login.invalid-email-address'));
						\Response::redirect_back();
					}

					// what went wrong now?
					catch(\Exception $e)
					{
						// log the error so an administrator can have a look
						logger(\Fuel::L_ERROR, '*** Error sending email ('.__FILE__.'#'.__LINE__.'): '.$e->getMessage());

						\Message::error(__('login.error-sending-email'));
						\Response::redirect_back();
					}
				}
			}

			// posted form, but email address posted?
			else
			{
				// inform the user and fall through to the form
				\Message::error(__('login.error-missing-email'));
			}

			// inform the user an email is on the way (or not ;-))
			\Message::info(__('login.recovery-email-send'));
			\Response::redirect_back();
		}

		// no form posted, do we have a hash passed in the URL?
		elseif ($hash !== null)
		{
			// get the userid from the hash
			$user = substr($hash, 44);

			// and find the user with this id
			if ($user = \Model\Auth_User::find_by_id($user))
			{
				// do we have this hash for this user, and hasn't it expired yet (we allow for 24 hours response)?
				if (isset($user->lostpassword_hash) and $user->lostpassword_hash == $hash and time() - $user->lostpassword_created < 86400)
				{
					// invalidate the hash
					\Auth::update_user(
						array(
							'lostpassword_hash' => null,
							'lostpassword_created' => null
						),
						$user->username
					);

					// log the user in and go to the profile to change the password
					if (\Auth::instance()->force_login($user->id))
					{
						\Message::info(__('login.password-recovery-accepted'));
						\Response::redirect('profile');
					}
				}
			}

			// something wrong with the hash
			\Message::error(__('login.recovery-hash-invalid'));
			\Response::redirect_back();
		}

		// no form posted, and no hash present. no clue what we do here
		else
		{
			\Response::redirect_back();
		}
	}

	public function action_callback()
		{
			// Opauth can throw all kinds of nasty bits, so be prepared
			try
			{
				// get the Opauth object
				$opauth = \Auth_Opauth::forge(false);

				// and process the callback
				$status = $opauth->login_or_register();

				// fetch the provider name from the opauth response so we can display a message
				$provider = $opauth->get('auth.provider', '?');

				// deal with the result of the callback process
				switch ($status)
				{
					// a local user was logged-in, the provider has been linked to this user
					case 'linked':
						// inform the user the link was succesfully made
						\Message::success(sprintf(__('login.provider-linked'), ucfirst($provider)));
						// and set the redirect url for this status
						$url = 'dashboard';
					break;

					// the provider was known and linked, the linked account as logged-in
					case 'logged_in':
						// inform the user the login using the provider was succesful
						\Message::success(sprintf(__('login.logged_in_using_provider'), ucfirst($provider)));
						// and set the redirect url for this status
						$url = 'dashboard';
					break;

					// we don't know this provider login, ask the user to create a local account first
					case 'register':
						// inform the user the login using the provider was succesful, but we need a local account to continue
						\Message::info(sprintf(__('login.register-first'), ucfirst($provider)));
						// and set the redirect url for this status
						$url = \Uri::create('authenticate/register');
					break;

					// we didn't know this provider login, but enough info was returned to auto-register the user
					case 'registered':
						// inform the user the login using the provider was succesful, and we created a local account
						\Message::success(__('login.auto-registered'));
						// and set the redirect url for this status
						$url = 'dashboard';
					break;

					default:
						throw new \FuelException('Auth_Opauth::login_or_register() has come up with a result that we dont know how to handle.');
				}

				// redirect to the url set
				\Response::redirect($url);
			}

			// deal with Opauth exceptions
			catch (\OpauthException $e)
			{
				\Message::error($e->getMessage());
				\Response::redirect_back();
			}

			// catch a user cancelling the authentication attempt (some providers allow that)
			catch (\OpauthCancelException $e)
			{
				// you should probably do something a bit more clean here...
				exit('It looks like you canceled your authorisation.'.\Html::anchor('authenticate/oath/'.$provider, 'Click here').' to try again.');
			}

	}		

	public function action_logout()
	{
		// remove the remember-me cookie, we logged-out on purpose
		\Auth::dont_remember_me();

		// logout
		\Auth::logout();

		// inform the user the logout was successful
		\Message::success(__('login.logged-out'));

		// and go back to where you came from (or the application
		// homepage if no previous page can be determined)
		\Response::redirect_back();
	}

	public function action_oauth($provider = null)
	{
	    // bail out if we don't have an OAuth provider to call
	    if ($provider === null)
	    {
	        \Message::error(__('login.no-provider-specified'));
	        \Response::redirect_back();
	    }

	    // load Opauth, it will load the provider strategy and redirect to the provider
	    \Auth_Opauth::forge();
	}
	
	/**
	 * Forgotten
	 * 
	 * Allows a user to change reset their password
	 *
	 * @return void
	 * @author James Pudney james@phpgenie.co.uk
	 **/
	public function action_forgotten($hash = null)
	{
		$form = null;
		$passwordResetFrom = false;
		$email_address = "";
		$user = null;
		
		if ( isset($hash) and ! \Input::post('reset') ) {
			
			try {
				
				// find the user
				$user = \Model\Auth_User::find_by_id(substr($hash, 44));
				$email = $user->email;
									
				//  is the token still valid?
				if (isset($user->lostpassword_hash) and $user->lostpassword_hash == $hash and time() - $user->lostpassword_created < 86400) {
					// time is valid
					$form = \Fieldset::forge('password_reset');
					
					$form->add('password', __('forgotten.form.password'), array('type' => 'password', 'class' => 'form-control'), array('required'));
					$form->add('confirm', __('forgotten.form.confirm'), array('type' => 'password', 'class' => 'form-control'), array(array('required_with', 'password')));
					$form->add('reset_password', __('forgotten.form.reset'), array('type' => 'button', 'class' => '', array('required')));
					$form->form()->add_csrf();		
					
					$passwordResetFrom = true;	
					
				} else {
					// time is not valid
					\Message::error(__('forgotten.token.expired'));
				}
			} catch (Exception $e) {
				\Log::warning($e->getMessage(), __METHOD__);
			}
			
		}	else {
			$form = \Fieldset::forge('forgotten');
		
			$form->add('email_address', __('forgotten.form.email_address'), array('type' => 'email', 'class' => 'form-control'), array('required'));
			$form->add('reset', __('forgotten.form.reset'), array('type' => 'button', 'class' => '', array('required')));
			$form->form()->add_csrf();		
		}
		
		if (\Input::post('reset')) {
			// handle the resetting of the users password
		
			// check validation
			if ($form->validation()->run()) {
			
				// check security
				if (\Security::check_token()) {
					
					if ($user = \Model\Auth_User::find_by_email($form->validated('email_address'))) {
						
						// send the email
						try {
						
							// generate a recovery hash
	            $hash = \Auth::instance()->hash_password(\Str::random()).$user->id;

	            // and store it in the user profile
	            \Auth::update_user(
	                array(
	                    'lostpassword_hash' => $hash,
	                    'lostpassword_created' => time()
	                ),
	                $user->username
	            );
					
							// load the email package
							\Package::load('email');
					
							// the url
							$url = \Uri::create(sprintf('authenticate/forgotten/%s', $hash));
					
							$email = \Email::forge();
					
							// Set the from address
							$email->from(__('forgotten.email.from.address'), 'Trsutev Shopify App Password Reset');

							// Set the to address
							$email->to($form->validated('email_address'));

							// Set a subject
							$email->subject('Trustev Shopify App: Password Reset');

							// And set the body.
							$email->body(__('forgotten.email.body', array('url' => $url)));
					
							$email->send();
					
							\Message::success(__('forgotten.success.sent', array('email_address' => $form->validated('email_address'))));
					
						} catch (Exception $e) {
							\Log::error($e->getMessage(), __METHOD__);
							\Message::error(__('forgotten.failed.email'));
						}
					} else {
						\Messages::error(__('forgotten.not.found'));
					}
					
				} else {
					// security failed
					\Log::warning('Security failed', __METHOD__);
					\Message::error(__('forgotten.failed.security'));
				}
			
			} else {
				// validation failed
				\Log::warning('Validation failed', __METHOD__);
				\Message::error(__('forgotten.failed.validation'));
			}
		
		} else if (\Input::post('reset_password')) {
			// check validation
			if ($form->validation()->run()) {
			
				// check security
				if (\Security::check_token()) {
			
					// find the user
					if ($user) {
						
						if (\Auth::change_password(\Auth::reset_password($user->username), $form->validated('password'), $user->username)) {
							
							// clear the hash, etc
	            \Auth::update_user(
	                array(
	                    'lostpassword_hash' => null,
	                    'lostpassword_created' => null
	                ),
	                $user->username
	            );
							
							// tell the user their password has changed
							\Message::success(__('forgotten.password.changed'));
							
							// force the login, only if they have a shop in the session
							if (\Auth::instance()->force_login($user->id) && \Session::get('shop', false)) {
              	
              }
							
							\Response::redirect('dashboard');
							
						} else {
							\Message::error(__('forgotten.password.not_changed'));
						}
						
					} else {
						\Message::error(__('forgotten.user.not_found'));
					}
			
				} else {
					// security failed
					\Log::warning('Security failed', __METHOD__);
					\Message::error(__('forgotten.failed.security'));
				}
			
			} else {
				// email failed
				\Log::warning('Validation failed', __METHOD__);
				\Message::error(__('forgotten.failed.reset_password'));
			}
		}
		
		// display the login page
		$view = \View::forge('authenticate::forgotten/forgotten');
		$view->set('messages', \Message::get(), false);
		$view->set('form', $form, false);
		$view->set('passwordResetFrom', $passwordResetFrom, false);

		$this->template->pageTitle = "Reset Forgotten Password";
		$this->template->content = $view;
		$this->template->set('messages', \Message::get(), false);
	}

	protected function link_provider($userid)
	{
		// do we have an auth strategy to match?
		if ($authentication = \Session::get('auth-strategy.authentication', array()))
		{
			// don't forget to pass false, we need an object instance, not a strategy call
			$opauth = \Auth_Opauth::forge(false);

			// call Opauth to link the provider login with the local user
			$insert_id = $opauth->link_provider(array(
				'parent_id' => $userid,
				'provider' => $authentication['provider'],
				'uid' => $authentication['uid'],
				'access_token' => $authentication['access_token'],
				'secret' => $authentication['secret'],
				'refresh_token' => $authentication['refresh_token'],
				'expires' => $authentication['expires'],
				'created_at' => time(),
			));
		}
	}	

	protected function getRegistrationForm() {
		// create the registration fieldset
		$form = \Fieldset::forge('registerform');

		$form->add('fullname', __('login.form.fullname'), array('type' => 'text', 'class' => 'text'), array('required'));
		$form->add('username', __('login.form.username'), array('type' => 'text', 'class' => 'text'), array('required'));
		$form->add('email', __('login.form.email'), array('type' => 'email', 'class' => 'text'), array('required'));
		$form->add('password', __('login.form.password'), array('type' => 'password', 'class' => 'text'), array('required'));
		$form->add('confirm', __('login.form.confirm'), array('type' => 'password', 'class' => 'text'), array('required'));

		$form->add('register', '', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => __('login.form.button.register')));

		return $form;
	}
}